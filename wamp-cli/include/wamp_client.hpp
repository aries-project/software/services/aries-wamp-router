/**
 * Copyright (C) 2020 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: main.cpp
 * @brief Nexus WAMP router
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef NEXUS_WAMP_CLIENT_HPP
#define NEXUS_WAMP_CLIENT_HPP

#include <nexus/wamp/backend_service.hpp>
#include <nexus/wamp/session.hpp>
#include <string>

namespace nexus
{

class Client : public BackendService
{
public:
    Client(const std::string &env_file);

    /**
     *
     */
    void onConnect() override;

    /**
     *
     */
    void onDisconnect() override;

    /**
     *
     */
    void onJoin() override;
};

} // namespace nexus

#endif /* NEXUS_WAMP_CLIENT_HPP */